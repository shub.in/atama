# ATAMA MOTORS

## Установка проекта
```bash
git clone ...
cd atama
npm install
```

### Запуск проекта для разработки
```bash
npm run serve
```

### Сборка проекта на продакшн
```
npm run build
```
