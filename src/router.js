import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/catalog',
      name: 'parts',
      component: () => import(/* webpackChunkName: "part" */ './views/Parts.vue')
    },
    {
      path: '/catalog/:id',
      name: 'part',
      component: () => import(/* webpackChunkName: "parts" */ './views/Part.vue')
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }

})
