import Vue from 'vue'
import Vuex from 'vuex'
import api from './api'
import cart from './cart'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    api,
    cart
  }
})
