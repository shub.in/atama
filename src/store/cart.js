import axois from 'axios';

export const HTTP = axois.create({
  baseURL: ''
});


export default {
  namespaced: true,
  state: {
    items: [],
    contacts: {
      name: '',
      phone: '',
      email: '',
      comment: ''
    },
    part: {
      mark: '',
      model: '',
      body: '',
      engine: '',
      year: ''
    },
    is_show: false,
    is_show_order: false,
    is_show_part: false,
    cart_sending: false,
    part_sending: false
  },
  getters: {
    cart_length: state => {
      return state.items.length;
    },
    in_cart: state => part => {
      return !!state.items.filter(item => item.id === part.id).length;
    }
  },
  mutations: {
    init(state) {
      state.items = JSON.parse(localStorage.getItem('cart')) || [];
      if(localStorage.getItem('part')) {
        state.part = JSON.parse(localStorage.getItem('part'));
      }
      if(localStorage.getItem('contacts')) {
        state.contacts = JSON.parse(localStorage.getItem('contacts'));
      }
    },
    add2cart(state, item) {
      if (!state.items.filter(part => part.id === item.id).length) {
        item.amount = 1;
        state.items.push(item);
        localStorage.setItem('cart', JSON.stringify(state.items));
      }
    },
    remove4cart(state, item) {
      state.items = state.items.filter(part => part.id !== item.id);
      localStorage.setItem('cart', JSON.stringify(state.items));
    },
    show_cart(state) {
      document.getElementsByTagName('body')[0].classList.add('overflow-h')
      state.is_show = true;
    },
    hide_cart(state) {
      document.getElementsByTagName('body')[0].classList.remove('overflow-h')
      state.is_show = false;
    },
    show_cart_order(state) {
      state.is_show = false;
      state.is_show_order = true;
    },
    hide_cart_order(state) {
      document.getElementsByTagName('body')[0].classList.remove('overflow-h')
      state.is_show_order = false;
    },
    show_part(state) {
      document.getElementsByTagName('body')[0].classList.add('overflow-h')
      state.is_show_part = true;
    },
    hide_part(state) {
      document.getElementsByTagName('body')[0].classList.remove('overflow-h')
      state.is_show_part = false;
    },
    update_part_amount(state, e) {
      if(!!state.items.filter(item => item.id === e.target.dataset.id).length) {
        state.items.filter(item => item.id === e.target.dataset.id)[0].amount = e.target.value;
        localStorage.setItem('cart', JSON.stringify(state.items));
      }
    },
    update_contacts(state, e) {
      state.contacts[e.target.name] = e.target.value;
      localStorage.setItem('contacts', JSON.stringify(state.contacts));
    },
    update_part(state, e) {
      state.part[e.target.name] = e.target.value;
      localStorage.setItem('part', JSON.stringify(state.part));
    },
    clear_part(state) {
      Object.keys(state.part).map(item => state.part[item] = '');
      localStorage.setItem('part', JSON.stringify(state.part));
    },
    clear_cart(state) {
      state.items = [];
      localStorage.setItem('cart', JSON.stringify(state.items));
    },
    toggle_cart(state, value=false) {
      state.cart_sending = value;
    },
    toggle_part(state, value=false) {
      state.part_sending = value;
    }
  },
  actions: {
    send_cart({state, commit}) {
      commit('toggle_cart', true);
      return HTTP.post('/cart', {parts: state.items, contacts: state.contacts})
        .then(response => {
          commit('clear_cart');
          commit('toggle_cart');
          commit('hide_cart_order');
          return response;
        }).catch(error => {return error});
    },
    send_part({state, commit}) {
      commit('toggle_part', true);
      return HTTP.post('/part', {part: state.part, contacts: state.contacts})
        .then(response => {
          commit('clear_part');
          commit('toggle_part');
          commit('hide_part');
          return response;
        })
        .catch(error => {return error});
    }
  }
};
