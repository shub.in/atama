import axois from 'axios';

export const HTTP = axois.create({
  baseURL: 'https://bibinet.ru/service',
  params: {
    ver_api: 'v3',
    ver2: 1,
    format: 'json',
    branch: 385,
    type_search: 'contracts'
  }
});

export default {
  namespaced: true,
  state: {
    parts: [],
    part: {},
    page: 0,
    loading: false,
    the_end: false,
    values: {
      part_type: null,
      mark: null,
      model: null,
      frame: null,
      engine: null,
      price_from: null,
      price_to: null
    },
    options: {
      part_type: [],
      mark: [],
      model: [],
      frame: [],
      engine: []
    },
    loadings: {
      part_type: false,
      mark: false,
      model: false,
      frame: false,
      engine: false
    }
  },
  mutations: {
    set_part(state, part={}) {
      state.part = part;
    },
    set_parts(state, parts=[]) {
      state.parts = parts;
      state.the_end = false;
    },
    add_parts(state, parts=[]) {
      if (parts.length) {
        state.parts = state.parts.concat(parts);
        state.the_end = false;
      } else {
        state.the_end = true;
      }
    },
    set_page(state, page=1) {
      state.page = page;
    },
    set_reference(state, payload) {
      state.options[payload.type] = payload.data;
    },
    set_value(state, payload) {
      state.values[payload.field] = payload.value || null;
    },
    unset_mark(state) {
      state.options.model = [];
      state.values.model = null;
    },
    unset_model(state) {
      state.options.frame = [];
      state.options.engine = [];
      state.values.frame = null;
      state.values.engine = null;
    },
    toggle_loading(state, value) {
      state.loading = value;
    }
  },
  actions: {
    select_part_type({ commit, dispatch }, value=null) {
      commit('set_value', {field: 'part_type', value: value});
      dispatch('get_parts');
    },
    select_mark({ commit, dispatch }, value=null) {
      commit('set_value', {field: 'mark', value: value});
      commit('unset_mark');
      commit('unset_model');
      dispatch('get_parts');
    },
    select_model({ commit, dispatch }, value=null) {
      commit('set_value', {field: 'model', value: value});
      commit('unset_model');
      dispatch('get_parts');
    },
    select_frame({ commit, dispatch }, value=null) {
      commit('set_value', {field: 'frame', value: value});
      dispatch('get_parts');
    },
    select_engine({ commit, dispatch }, value=null) {
      commit('set_value', {field: 'engine', value: value});
      dispatch('get_parts');
    },
    set_price({ dispatch, state }, e) {
      state.values[e.target.name] = e.target.value;
      dispatch('get_parts');
    },
    get_part({ commit }, id) {
      HTTP.get('/search/used_parts', {params: {invnn: id}})
        .then(response => commit('set_part', response.data.data[0]))
        .catch(error => console.log('Error: ', error.response.statusText));
    },
    get_parts({ commit, state }, add=false) {
      commit('toggle_loading', true);
      let mutation = 'add_parts';
      commit('set_page', state.page + 1);
      if (!add) {
        mutation = 'set_parts';
        commit('set_page');
      }
      let flt = {page: state.page};
      Object.keys(state.values)
        .filter(item => state.values[item] != null)
        .reduce((obj, key) => {
          if (['price_from', 'price_to'].includes(key)) {
            obj[key] = state.values[key];
          } else {
            obj[key] = state.values[key][0];
          }
          return obj;
        }, flt);
      const ppp = Object.keys(flt).length > 1 ? 10 : 5;
      HTTP.get('/search/used_parts', {params: {...flt, parts_per_page: ppp}})
        .then(response => {
          commit(mutation, response.data.data);
          commit('toggle_loading', false);
        }).catch(error => console.log('Error: ', error.response.statusText));
    },
    get_reference({ commit, state }, type='part_type') {
      let params = { is_used_part: 1, variants: type, loading: false };
      if (type === 'model' && state.values.mark != null) {
        params.mark = state.values.mark[0];
        params.loading = true;
      }
      if (['engine', 'frame'].includes(type) && state.values.model != null) {
        params.mark = state.values.mark[0];
        params.model = state.values.model[0];
        params.loading = true;
      }
      if ((!state.options[type].length && !['model', 'engine', 'frame'].includes(type))
        || (['model', 'engine', 'frame'].includes(type) && params.loading)) {
        state.loadings[type] = true;
        HTTP.get('/get_reference/', { params: params })
          .then(response => {
            commit(`set_reference`, {type: type, data: response.data});
            state.loadings[type] = false;
          }).catch(error => console.log('Error: ', error.response.statusText));
      }
    }

  }
};
