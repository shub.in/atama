import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './filters'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps)

new Vue({
  router,
  store,
  beforeCreate () {
    this.$store.commit('cart/init')
  },
  render: h => h(App)
}).$mount('#app')
